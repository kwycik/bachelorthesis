import numpy as np
import pandas as pd

from stable_baselines3 import DDPG, SAC, TD3, A2C
from stable_baselines3.common.env_checker import check_env

from torch.utils.tensorboard import SummaryWriter

from classes import ProsumerEnv


columns = [
    'Demand (kWh)', 'PV Supply (kWh)', 
       
    'Import Price (EUR/kWh)', 'Air Temp. (°C)', 'Rel. Humidity (%)', 'Precip. Depth (mm)', 'Pressure (hPa)', 
    'Global Radiation (J/cm)', 'Sun Duration (min)', 'Wind Speed (m/sec)',
           
    'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15', 'hour_16', 'hour_17', 'hour_18', 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23', 
    
    'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6', 'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12', 
    
    'weekday_0', 'weekday_1', 'weekday_2', 'weekday_3', 'weekday_4', 'weekday_5', 'weekday_6',
]


# Importing Dataframe for Res1:
df = pd.read_csv('./Dataframes/Res1.csv', 
                     names = columns, 
                     sep = ',')

df = df.drop(df.index[0], axis = 0)

for col in df:
    df[col] = pd.to_numeric(df[col])


# Importing Dataframe for Res4:
df_eval = pd.read_csv('./Dataframes/Res4.csv', 
                      names = columns, 
                      sep = ',')

df_eval = df_eval.drop(df_eval.index[0], axis = 0)

for col in df_eval:
    df_eval[col] = pd.to_numeric(df_eval[col])


env = ProsumerEnv.ProsumerEnv(df)
test_env = ProsumerEnv.ProsumerEnv(df_eval)

check_env(env)
check_env(test_env)

time = pd.Timestamp.now().strftime('%Y-%m-%d %X')
model_name = 'DDPG_{}'.format(time)
model_name_zero = '{}_0'.format(model_name)

DDPG_models_dir = './training_version10/DDPG'
save_path = f'{DDPG_models_dir}/{model_name}'

log_dir = './training_version10/logs'
log_path = f'{log_dir}/{model_name_zero}'

model = DDPG('MlpPolicy', env, verbose = 1, tensorboard_log = log_dir)
    

writer = SummaryWriter(log_path)
timesteps_per_episode = 10000
for i in range(1, 100 + 1):
    model.learn(total_timesteps = timesteps_per_episode, reset_num_timesteps = False, tb_log_name = model_name)
    model.save(f'{save_path}/{timesteps_per_episode * i}')
    
    episodes = 5
    total_bill_arr = []
    rew_arr = []
    for ep in range(1, episodes + 1):
        done = False
        obs, _ = test_env.reset()
    
        while not done:
            action = model.predict(obs)
        
            obs, reward, done, _, info = test_env.step(action)
            
            rew_arr.append(reward)
    
            if done:
                total_bill_arr.append(info['electricity_bill'])
                
           
    rew_arr = np.array(rew_arr)
    mean_reward_test = np.mean(rew_arr)
            
    total_bill_arr = np.array(total_bill_arr)
    mean_bill_test = np.mean(total_bill_arr)
    
    writer.add_scalar("MeanReward/train", mean_reward_test, timesteps_per_episode * i)
    writer.add_scalar("MeanTotalBill/train", mean_bill_test, timesteps_per_episode * i)
    
writer.flush()
writer.close()