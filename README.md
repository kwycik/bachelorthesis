# Bachelor Thesis in Information Systems at the University of Cologne

Code for the bachelor thesis of Kai Wycik – Summer Semester of 2023. The thesis delves into utilising explainable reinforcement learning in order to better understand how to optimally consume, sell, and store electricity in prosumer households.





## Repository Contents:
### Jupyter Notebooks
This repository contains five Jupyter notebooks. The Jupyter notebooks provide insight into the technical details of the bachelor thesis as well as documenting the decisions made throughout the implementation. The "DataStudiesAndPrep" notebook documents the data preperation, i.e. how the different datasets were aggregated and combined in order to derive at the two final household datasets for household one and four. The "ModelTraining" notebook documents the training process of the different reinforcement learning models. Finally, the "ModelExplaining _model_" notebooks document the model explaining process, as well as the results of the different model explainations.  

### Python Files
The python files are used as an alternative to the "ModelTraining" notebook. If the model training process is executed via the "ModelTraining" notebook, only a single model can be trained at a time. In order to train multiple models at the same time, pure python files are provided.

### Dataframes
The dataframes folder provides the datasets for the two households on which the training and evaluation environments are based on. Res1.csv is the dataset for household one, which is used as a training dataset. Res4.csv is the dataset for household four, which is used as a evaluation dataset.

### Shapley Values
This folder provides the shapley values, as well as the sampled datasets (and for the DDPG model and PPO model with a recurrent neural network structure some exported plots from the "ModelExplaining _model_" notebooks) for the individual models.

### Classes
The classes folder provides some python classes for the PPO models (with and without a recurrent neural network strucutre) and for the prosumer environment which the reinforcement learning agents act in. The code for the PPO classes is based on Yu (2020).

### training_version10/Best Models
This folder contains the three best models which emerged in the tenth iteration of model training as well as the TensorBoard logs for these three models.





## How to use the files:
To execute the files in the repository, download the complete repository and make sure all necessary packages are installed. 

### DataStudiesAndPrep
To execute this notebook, make sure to download the necessary datasets from the sources (see Deutscher Wetterdienst (n.d.), Ember (2023), and Open Power System Data (2020)). Make sure the dataset files are named as they are named in the code for the notebook. Then, execute the cells in the notebook. The output of these notebooks is saved in the dataframes folder of this repository. Thus, executing this notebook can be skipped.

### ModelTraining
Make sure the datasets for the environments are located in the dataframes folder and the classes are located in the classes folder. Then execute the cells of this notebook. Alternatively, one can also execture the pure python training files. 
To monitor the logs of the model which are being trained, start a TensorBoard window in the respective log folder.

### ModelExplaining
Make sure the model which are to be explained are located in the respective folders as well as all dataframes and classes. Re-calculating the Shapley values for the respective models can be done, is however not necessary (note that calculating the Shapley values, especially with KernelSHAP, can take a lot of time). The calculated Shapley values can also simply be taken from the respective dataframes in the Shapley values folder.



## References
Deutscher Wetterdienst. (n.d.). Climate Data Center - Open Data. Retrieved June 17, 2023, from https://www.dwd.de/DE/leistungen/opendata/opendata.html?nn=342666

Ember. (2023, May 5). European Wholesale Electricity Price Data. Retrieved June 13, 2023, from https://ember-climate.org/data-catalogue/european-wholesale-electricity-price-data/

Open Power System Data. (2020, April 14). Household Data. Retrieved June 13, 2023, from https://data.open-power-system-data.org/household_data/2020-04-15

Yu, E. Y. (2020). PPO-for-Beginners. GitHub. Retrieved June 4, 2023, from https://github.com/ericyangyu/PPO-for-Beginners
