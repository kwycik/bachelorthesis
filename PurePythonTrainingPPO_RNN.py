import numpy as np
import pandas as pd

from stable_baselines3.common.env_checker import check_env

from classes import ProsumerEnv, PPO_RNN


columns = [
    'Demand (kWh)', 'PV Supply (kWh)', 
       
    'Import Price (EUR/kWh)', 'Air Temp. (°C)', 'Rel. Humidity (%)', 'Precip. Depth (mm)', 'Pressure (hPa)', 
    'Global Radiation (J/cm)', 'Sun Duration (min)', 'Wind Speed (m/sec)',
           
    'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15', 'hour_16', 'hour_17', 'hour_18', 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23', 
    
    'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6', 'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12', 
    
    'weekday_0', 'weekday_1', 'weekday_2', 'weekday_3', 'weekday_4', 'weekday_5', 'weekday_6',
]


# Importing Dataframe for Res1:
df = pd.read_csv('./Dataframes/Res1.csv', 
                 names = columns, 
                 sep = ',')

df = df.drop(df.index[0], axis = 0)

for col in df:
    df[col] = pd.to_numeric(df[col])


# Importing Dataframe for Res4:
df_eval = pd.read_csv('./Dataframes/Res4.csv', 
                      names = columns, 
                      sep = ',')

df_eval = df_eval.drop(df_eval.index[0], axis = 0)

for col in df_eval:
    df_eval[col] = pd.to_numeric(df_eval[col])


env = ProsumerEnv.ProsumerEnv(df)
test_env = ProsumerEnv.ProsumerEnv(df_eval)

check_env(env)
check_env(test_env)


# min: 512 | max: 5120 --> mean: 2816 
timesteps_per_batch = 2048
    
# min: 4 | max: 80 --> mean: 42
timesteps_per_episode_mult = 40

total_train = 1000000
    
time = pd.Timestamp.now().strftime('%Y-%m-%d %X')
model_name = 'PPO_RNN_{}'.format(time)

model = PPO_RNN.PPO_RNN(env, test_env, model_name, timesteps_per_batch, timesteps_per_episode_mult)
model.learn(total_train)

del model