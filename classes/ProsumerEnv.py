import numpy as np
import pandas as pd
import gymnasium as gym

from gymnasium.spaces import Box 


class ProsumerEnv(gym.Env):
    """Custom Environment that follows gym interface."""
    metadata = {"render_modes": ["human"], "render_fps": 30}
    

    def __init__(self, df):
        super(ProsumerEnv, self).__init__()
        
        # Define action and observation space
        self.action_space = Box(low = 0, 
                                high = 1,
                                shape = (5, ), 
                                dtype = np.float32)
        
        self.observation_space = Box(low = -float("inf"), 
                                     high = float("inf"),
                                     shape = (54, ), 
                                     dtype = np.float32)
        
        # see https://www.verbraucherzentrale.de/wissen/energie/erneuerbare-energien/eeg-2023-das-hat-sich-fuer-photovoltaikanlagen-geaendert-75401
        self.electricity_export_price = 0.071 # EUR/kWh
        
        self.df = df
        
        self.storage_cap = 100 # kWh
        
        self.mean_arr = []
        self.std_arr = []
        
        features = [
            'Demand (kWh)', 'PV Supply (kWh)', 
       
            'Import Price (EUR/kWh)', 'Air Temp. (°C)', 'Rel. Humidity (%)', 'Precip. Depth (mm)', 'Pressure (hPa)', 
            'Global Radiation (J/cm)', 'Sun Duration (min)', 'Wind Speed (m/sec)',
            
            'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15', 'hour_16', 'hour_17', 'hour_18', 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23', 
    
            'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6', 'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12', 
    
            'weekday_0', 'weekday_1', 'weekday_2', 'weekday_3', 'weekday_4', 'weekday_5', 'weekday_6',
        ]
        
        self.setMeanAndStd(features)
        
        
    def step(self, action):
        # get actions as deviations from base policy
        # action = self.getBasePolicyActions(action)
        
        # get the seven flows of electricity & stored_electricity_left
        supply_to_demand, storage_to_demand, grid_to_demand, storage_to_grid, supply_to_storage, supply_to_grid, grid_to_storage, storage = self.convertActions(action)   
        
        # calculate electricity bill:
        bill_for_this_hour = ((grid_to_storage + grid_to_demand) * self.state['price_import']) - ((supply_to_grid + storage_to_grid) * self.electricity_export_price)
        bill = self.state['electricity_bill'] + bill_for_this_hour
        
        # calculate reward:
        self.reward = -bill_for_this_hour + 5
        
        # if not done yet, get new obs 
        self.time += 1
        
        if self.time < len(self.df):
            self.setState(self.time, storage, bill)
        
            self.setObservation()
        else:
            self.done = True
        
        
        info = {'electricity_bill': self.state['electricity_bill']}
        truncated = False
            
        return self.observation, self.reward, self.done, truncated, info

    
    def reset(self, **kwargs):
        self.done = False
        
        self.time = 0
        
        self.setState(self.time, 0, 0)
        
        self.setObservation()
        
        info = {'electricity_bill': self.state['electricity_bill']}
        
        return self.observation, info
    
    
    def convertActions(self, action):
        if isinstance(action[0], np.ndarray):
            action = action[0]

         # Calculate the 7 actions from the 5 decisions made. The 7 actions are:
            # 7: Grid --> Storage
            # 3: Grid --> Demand
            
            # 6: PV Supply --> Grid
            # 5: PV Supply --> Storage
            # 1: PV Supply --> Demand
            
            # 2: Storage --> Demand
            # 4: Storage --> Grid

        # 1: PV Supply --> Demand:
        supply_to_demand = min(action[0] * self.state['electricity_demand'], self.state['pv_electricity_supply'])
        pv_supply_left = self.state['pv_electricity_supply'] - supply_to_demand
        demand_left = self.state['electricity_demand'] - supply_to_demand
        
        # 2: Storage --> Demand:
        storage_to_demand = min(action[1] * demand_left, self.state['electricity_stored'])
        demand_left -= storage_to_demand
        stored_electricity_left = self.state['electricity_stored'] - storage_to_demand
        free_storage_capacity = self.storage_cap - stored_electricity_left
        
        # 3: Grid --> Demand:
        grid_to_demand = demand_left
        
        # 4: Storage --> Grid:
        storage_to_grid = action[3] * stored_electricity_left
        stored_electricity_left -= storage_to_grid
        free_storage_capacity = self.storage_cap - stored_electricity_left
        
        # 5: PV Supply --> Storage:
        supply_to_storage = min(action[2] * pv_supply_left, free_storage_capacity)
        pv_supply_left -= supply_to_storage
        stored_electricity_left += supply_to_storage
        free_storage_capacity = self.storage_cap - stored_electricity_left
        
        # 6: PV Supply --> Grid:
        supply_to_grid = pv_supply_left
        
        # 7: Grid --> Storage:
        grid_to_storage = action[4] * free_storage_capacity
        stored_electricity_left += grid_to_storage
        
        return supply_to_demand, storage_to_demand, grid_to_demand, storage_to_grid, supply_to_storage, supply_to_grid, grid_to_storage, stored_electricity_left
        
        
    def setState(self, t, storage, bill):
        self.state = {
            'pv_electricity_supply': self.df['PV Supply (kWh)'].iloc[t],
            'electricity_demand': self.df['Demand (kWh)'].iloc[t],
            'price_import': self.df['Import Price (EUR/kWh)'].iloc[t],
            'air_temp': self.df['Air Temp. (°C)'].iloc[t], 
            'rel_humidity': self.df['Rel. Humidity (%)'].iloc[t],  
            'precip_depth': self.df['Precip. Depth (mm)'].iloc[t], 
            'pressure': self.df['Pressure (hPa)'].iloc[t], 
            'global_radiation': self.df['Global Radiation (J/cm)'].iloc[t], 
            'sun_duration': self.df['Sun Duration (min)'].iloc[t], 
            'wind_speed': self.df['Wind Speed (m/sec)'].iloc[t],
            
            'hour_0': self.df['hour_0'].iloc[t], 'hour_1': self.df['hour_1'].iloc[t], 'hour_2': self.df['hour_2'].iloc[t], 'hour_3': self.df['hour_3'].iloc[t], 'hour_4': self.df['hour_4'].iloc[t], 'hour_5': self.df['hour_5'].iloc[t], 'hour_6': self.df['hour_6'].iloc[t], 'hour_7': self.df['hour_7'].iloc[t], 'hour_8': self.df['hour_8'].iloc[t], 'hour_9': self.df['hour_9'].iloc[t], 'hour_10': self.df['hour_10'].iloc[t], 'hour_11': self.df['hour_11'].iloc[t], 'hour_12': self.df['hour_12'].iloc[t], 'hour_13': self.df['hour_13'].iloc[t], 'hour_14': self.df['hour_14'].iloc[t], 'hour_15': self.df['hour_15'].iloc[t], 'hour_16': self.df['hour_16'].iloc[t], 'hour_17': self.df['hour_17'].iloc[t], 'hour_18': self.df['hour_18'].iloc[t], 'hour_19': self.df['hour_19'].iloc[t], 'hour_20': self.df['hour_20'].iloc[t], 'hour_21': self.df['hour_21'].iloc[t], 'hour_22': self.df['hour_22'].iloc[t], 'hour_23': self.df['hour_23'].iloc[t],
            
            'month_1': self.df['month_1'].iloc[t], 'month_2': self.df['month_2'].iloc[t], 'month_3': self.df['month_3'].iloc[t], 'month_4': self.df['month_4'].iloc[t], 'month_5': self.df['month_5'].iloc[t], 'month_6': self.df['month_6'].iloc[t], 'month_7': self.df['month_7'].iloc[t], 'month_8': self.df['month_8'].iloc[t], 'month_9': self.df['month_9'].iloc[t], 'month_10': self.df['month_10'].iloc[t], 'month_11': self.df['month_11'].iloc[t], 'month_12': self.df['month_12'].iloc[t],
            
            'weekday_0': self.df['weekday_0'].iloc[t], 'weekday_1': self.df['weekday_1'].iloc[t], 'weekday_2': self.df['weekday_2'].iloc[t], 'weekday_3': self.df['weekday_3'].iloc[t], 'weekday_4': self.df['weekday_4'].iloc[t], 'weekday_5': self.df['weekday_5'].iloc[t], 'weekday_6': self.df['weekday_6'].iloc[t],
            
            'electricity_stored': storage,
            'electricity_bill': bill
        }
        
        
    def setObservation(self):
        self.observation = [
            self.state['pv_electricity_supply'], self.state['electricity_demand'], 
            self.state['price_import'],  
                                
            self.state['air_temp'], self.state['rel_humidity'], 
            self.state['precip_depth'], self.state['pressure'],
            self.state['global_radiation'], self.state['sun_duration'], 
            self.state['wind_speed'], 
            
            self.state['hour_0'], self.state['hour_1'], self.state['hour_2'], self.state['hour_3'], self.state['hour_4'], self.state['hour_5'], self.state['hour_6'], self.state['hour_7'], self.state['hour_8'], self.state['hour_9'], self.state['hour_10'], self.state['hour_11'], self.state['hour_12'], self.state['hour_13'], self.state['hour_14'], self.state['hour_15'], self.state['hour_16'], self.state['hour_17'], self.state['hour_18'], self.state['hour_19'], self.state['hour_20'], self.state['hour_21'], self.state['hour_22'], self.state['hour_23'],
            
            self.state['month_1'], self.state['month_2'], self.state['month_3'], self.state['month_4'], self.state['month_5'], self.state['month_6'], self.state['month_7'], self.state['month_8'], self.state['month_9'], self.state['month_10'], self.state['month_11'], self.state['month_12'],
            
            self.state['weekday_0'], self.state['weekday_1'], self.state['weekday_2'], self.state['weekday_3'], self.state['weekday_4'], self.state['weekday_5'], self.state['weekday_6'],
                                
            self.state['electricity_stored']
        ]
        
        self.observation = self.normal(self.observation)
            
        self.observation = np.array(self.observation, dtype = np.float32)
        
        
    def normal(self, observation):
        normalized_obs_arr = []
        
        for i in range(10):
            normalized_obs = (observation[i] - self.mean_arr[i]) / self.std_arr[i]
            
            normalized_obs_arr.append(normalized_obs)
        
        for i in range(10, len(observation) - 1):
            normalized_obs_arr.append(observation[i]) # append time features
            
        normalized_stored_electricity = observation[-1] / self.storage_cap
        normalized_obs_arr.append(normalized_stored_electricity)
        
        return normalized_obs_arr
        
        
    def setMeanAndStd(self, features):
        for i in range(10):
            self.mean_arr.append(self.df[features[i]].mean())
            self.std_arr.append(self.df[features[i]].std())
        

    def activation_function(self, x):
        return x**2
    
    
    def getBasePolicyActions(self, action):
        base_policy = [1, 1, 1, 0, 0]
        action_arr = [0, 0, 0, 0, 0]
        
        action_arr[0] = base_policy[0] - self.activation_function(action[0])
        action_arr[1] = base_policy[1] - self.activation_function(action[1])
        action_arr[2] = base_policy[2] - self.activation_function(action[2])
        action_arr[3] = base_policy[3] + self.activation_function(action[3])
        action_arr[4] = base_policy[4] + self.activation_function(action[4])
        
        return action_arr