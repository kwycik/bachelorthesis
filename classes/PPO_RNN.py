import torch
import torch.nn.functional as F
import torch.optim.lr_scheduler as lr_scheduler

import gymnasium as gym
import numpy as np
import os


from torch import nn
from torch.optim import Adam
from torch.distributions import MultivariateNormal
from torch.utils.tensorboard import SummaryWriter


class RecursiveNN(nn.Module):
    def __init__(self, in_dim, out_dim, batch_size, model_type):
        super(RecursiveNN, self).__init__()
        self.model_type = model_type
        self.batch_size = batch_size

        if self.model_type == 'actor':
            self.rnn = nn.RNN(in_dim, 128, nonlinearity = 'relu')
            self.layer2 = nn.Linear(128, 128)
            self.layer3 = nn.Linear(128, 128)
            self.layer4 = nn.Linear(128, out_dim)
            self.parse = nn.Sigmoid()

        if self.model_type =='critic':
            self.layer1 = nn.Linear(in_dim, 128)
            self.layer2 = nn.Linear(128, 128)
            self.layer3 = nn.Linear(128, 128)
            self.layer4 = nn.Linear(128, out_dim)

            

    def forward(self, obs, batched = True):
        if isinstance(obs, tuple):
            obs =  obs[0]
            
        if isinstance(obs, np.ndarray):
            obs = torch.tensor(obs, dtype = torch.float)


        if self.model_type == 'actor' and batched:
            obs = torch.unsqueeze(obs, dim = 0)
            hidden_size = obs.shape[1]
            hidden = self.init_hidden_batched(hidden_size)

            x, hidden = self.rnn(obs, hidden)
            x = x.contiguous().view(-1, 128)

            x = F.relu(self.layer2(x))
            x = F.relu(self.layer3(x))
            x = self.layer4(x)
            output = self.parse(x)

        elif self.model_type == 'actor' and not batched:
            obs = torch.unsqueeze(obs, dim = 0)
            hidden = self.init_hidden()

            x, hidden = self.rnn(obs, hidden)
            x = x.contiguous().view(-1, 128)

            x = F.relu(self.layer2(x))
            x = F.relu(self.layer3(x))
            x = self.layer4(x)
            output = self.parse(x)

        elif self.model_type == 'critic':
            x = F.relu(self.layer1(obs))
            x = F.relu(self.layer2(x))
            x = F.relu(self.layer3(x))
            output = self.layer4(x)

        return output
    

    def init_hidden(self):
        hidden = torch.zeros(1, 128)

        return hidden
    

    def init_hidden_batched(self, hidden_size):
        hidden = torch.zeros(1, hidden_size, 128)

        return hidden
    

class PPO_RNN:
    def __init__(self, env, test_env, model_name, timesteps_per_batch, timesteps_per_episode_mult):
        # Make sure the environment is compatible with our code
        assert(type(env.observation_space) == gym.spaces.Box)
        assert(type(env.action_space) == gym.spaces.Box)

        # Initialize hyperparameters for training with PPO
        self._init_hyperparameters(timesteps_per_batch, timesteps_per_episode_mult)

        # Extract environment information
        self.env = env
        self.test_env = test_env
        self.obs_dim = env.observation_space.shape[0]
        self.act_dim = env.action_space.shape[0]

        # Initialize actor and critic networks
        self.actor = RecursiveNN(self.obs_dim, self.act_dim, self.batch_size, 'actor')                                                   # ALG STEP 1
        self.critic = RecursiveNN(self.obs_dim, 1, self.batch_size, 'critic')

        # Initialize optimizers for actor and critic
        self.actor_optim = Adam(self.actor.parameters(), lr = self.lr)
        self.critic_optim = Adam(self.critic.parameters(), lr = self.lr)
        self.actor_scheduler = lr_scheduler.LinearLR(self.actor_optim, total_iters = 100, start_factor = 1, end_factor = 0.001)
        self.critic_scheduler = lr_scheduler.LinearLR(self.critic_optim, total_iters = 100, start_factor = 1, end_factor = 0.001)
        
        # Initialize the covariance matrix used to query the actor for actions
        self.cov_var = torch.full(size = (self.act_dim, ), fill_value = 0.5)
        self.cov_mat = torch.diag(self.cov_var)
        
        self.log_dir = './training_version10/logs'
        self.ppo_models_dir = './training_version10/PPO_RNN'
        self.model_name = model_name
        
        self.log_path = f'{self.log_dir}/{self.model_name}'
        self.save_path = f'{self.ppo_models_dir}/{self.model_name}'
        
        if not os.path.exists(self.save_path):
            os.makedirs(self.save_path)
        
        if not os.path.exists(self.ppo_models_dir):
            os.makedirs(self.ppo_models_dir)
    
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        
    def learn(self, total_timesteps):
        self.writer = SummaryWriter(self.log_path)
        
        t_so_far = 0 # Timesteps simulated so far
        
        while t_so_far < total_timesteps:                                                                       # ALG STEP 2
            # Collecting batch simulations here
            batch_obs, batch_acts, batch_log_probs, batch_rews, batch_rtgs, batch_lens, total_bill_train = self.rollout()                     # ALG STEP 3

            # Calculate how many timesteps were collected this batch
            t_so_far += np.sum(batch_lens)

            # Calculate advantage at k-th iteration
            V, _ = self.evaluate(batch_obs, batch_acts)
            A_k = batch_rtgs - V.detach()                                                                       # ALG STEP 5

            # One of the only tricks I use that isn't in the pseudocode. Normalizing advantages
            # isn't theoretically necessary, but in practice it decreases the variance of 
            # our advantages and makes convergence much more stable and faster. I added this because
            # solving some environments was too unstable without it.
            A_k = (A_k - A_k.mean()) / (A_k.std() + 1e-10)

            # This is the loop where we update our network for some n epochs
            for _ in range(self.n_updates_per_iteration):                                                       # ALG STEP 6 & 7
                # Calculate V_phi and pi_theta(a_t | s_t)
                V, curr_log_probs = self.evaluate(batch_obs, batch_acts)

                # Calculate the ratio pi_theta(a_t | s_t) / pi_theta_k(a_t | s_t)
                # NOTE: we just subtract the logs, which is the same as
                # dividing the values and then canceling the log with e^log.
                ratios = torch.exp(curr_log_probs - batch_log_probs)

                # Calculate surrogate losses.
                surr1 = ratios * A_k
                surr2 = torch.clamp(ratios, 1 - self.clip, 1 + self.clip) * A_k

                # Calculate actor and critic losses.
                # NOTE: we take the negative min of the surrogate losses because we're trying to maximize
                # the performance function, but Adam minimizes the loss. So minimizing the negative
                # performance function maximizes it.
                actor_loss = (-torch.min(surr1, surr2)).mean()
                critic_loss = nn.MSELoss()(V, batch_rtgs)

                # Calculate gradients and perform backward propagation for actor network
                self.actor_optim.zero_grad()
                actor_loss.backward(retain_graph = True)
                self.actor_optim.step()

                # Calculate gradients and perform backward propagation for critic network
                self.critic_optim.zero_grad()
                critic_loss.backward()
                self.critic_optim.step()
                
            
            self.evalLog(t_so_far, total_timesteps, batch_rews, total_bill_train)
            
            # self.actor_scheduler.step()
            # self.critic_scheduler.step()
            
        
        self.writer.flush()
        self.writer.close()

                
    def rollout(self):
        # Batch data. For more details, check function header.
        batch_obs = []
        batch_acts = []
        batch_log_probs = []
        batch_rews = []
        batch_rtgs = []
        batch_lens = []
        total_bill_train = []

        t = 0 # Keeps track of how many timesteps we've run so far this batch

        # Keep simulating until we've run more than or equal to specified timesteps per batch
        while t < self.timesteps_per_batch:
            ep_rews = [] # rewards collected per episode

            # Reset the environment. Note that obs is short for observation. 
            obs = self.env.reset()
            obs = obs[0]
            done = False

            # Run an episode for a maximum of max_timesteps_per_episode timesteps
            for ep_t in range(self.max_timesteps_per_episode):
                t += 1 # Increment timesteps ran this batch so far

                # Track observations in this batch
                batch_obs.append(obs)

                # Calculate action and make a step in the env. 
                # Note that rew is short for reward.
                action, log_prob = self.get_action(obs)
                obs, rew, done, _, info = self.env.step(action)

                # Track recent reward, action, and action log probability
                ep_rews.append(rew)
                batch_acts.append(action)
                batch_log_probs.append(log_prob)

                # If the environment tells us the episode is terminated, break
                if done:
                    total_bill_train.append(info['electricity_bill'])
                    break

            # Track episodic lengths and rewards
            batch_lens.append(ep_t + 1)
            batch_rews.append(ep_rews)
            
        # Reshape data as tensors in the shape specified in function description, before returning
        batch_obs = torch.tensor(batch_obs, dtype = torch.float)
        batch_acts = torch.tensor(batch_acts, dtype = torch.float)
        batch_log_probs = torch.tensor(batch_log_probs, dtype = torch.float)
        
        batch_rtgs = self.compute_rtgs(batch_rews)      

        return batch_obs, batch_acts, batch_log_probs, batch_rews, batch_rtgs, batch_lens, total_bill_train

    
    def compute_rtgs(self, batch_rews):
        # The rewards-to-go (rtg) per episode per batch to return.
        # The shape will be (num timesteps per episode)
        batch_rtgs = []

        # Iterate through each episode
        for ep_rews in reversed(batch_rews):

            discounted_reward = 0 # The discounted reward so far

            # Iterate through all rewards in the episode. We go backwards for smoother calculation of each
            for rew in reversed(ep_rews):
                discounted_reward = rew + discounted_reward * self.gamma
                batch_rtgs.insert(0, discounted_reward)

        # Convert the rewards-to-go into a tensor
        batch_rtgs = torch.tensor(batch_rtgs, dtype = torch.float)

        return batch_rtgs

    
    def get_action(self, obs):
        # Query the actor network for a mean action
        mean = self.actor(obs, False)
        
        # Create a distribution with the mean action and std from the covariance matrix above.
        dist = MultivariateNormal(mean, self.cov_mat)

        # Sample an action from the distribution
        action = dist.sample()
        action = torch.clamp(action, min = 0.0, max = 1.0)

        # Calculate the log probability for that action
        log_prob = dist.log_prob(action)

        action = action.detach().numpy()
        log_prob = log_prob.detach()

        if isinstance(action[0], np.ndarray):
            action = action[0]

        # Return the sampled action and the log probability of that action in our distribution
        return action, log_prob

    
    def evaluate(self, batch_obs, batch_acts):
        # Query critic network for a value V for each batch_obs. Shape of V should be same as batch_rtgs
        V = self.critic(batch_obs, True).squeeze()

        # Calculate the log probabilities of batch actions using most recent actor network.
        mean = self.actor(batch_obs, True)
        dist = MultivariateNormal(mean, self.cov_mat)
        log_probs = dist.log_prob(batch_acts)

        # Return the value vector V of each observation in the batch
        # and log probabilities log_probs of each action in the batch
        return V, log_probs
    
    
    def predict(self, obs):
        action = self.actor(obs, False)

        action = action.detach().numpy()

        if isinstance(action[0], np.ndarray):
            action = action[0]
            
        return action
    
    
    def predict_batched(self, obs):
        action = self.actor(obs, True)

        action = action.detach().numpy()

        # if isinstance(action[0], np.ndarray):
        #     action = action[0]
            
        return action
            
    
    def evalLog(self, t, total_timesteps, batch_rews, total_bill_train):
        episodes = 5
        total_bill_arr = []
        rew_arr = []
        for ep in range(1, episodes + 1):
            done = False
            obs, _ = self.test_env.reset()
    
            while not done:
                action = self.predict(obs)
        
                obs, reward, done, _, info = self.test_env.step(action)
            
                rew_arr.append(reward)
    
                if done:
                    total_bill_arr.append(info['electricity_bill'])
            
        batch_rews = np.array(batch_rews)
        mean_reward_train = np.mean(batch_rews)
                
        total_bill_train = np.array(total_bill_train)
        mean_bill_train = np.mean(total_bill_train)
        
        rew_arr = np.array(rew_arr)
        mean_reward_test = np.mean(rew_arr)
            
        total_bill_arr = np.array(total_bill_arr)
        mean_bill_test = np.mean(total_bill_arr)
        
        rounded_percentage = "{:.2f}".format((t/total_timesteps) * 100)
        rounded_mean_reward_train = "{:.2f}".format(mean_reward_train)
        rounded_mean_reward_test = "{:.2f}".format(mean_reward_test)
        rounded_mean_bill_train = "{:.2f}".format(mean_bill_train)
        rounded_mean_bill_test = "{:.2f}".format(mean_bill_test)
        space = ' '
        print('Model Name: {} | Learning Progress: {}% | Mean of Rewards (Train): {} | Mean Total Electricity Bill (Train): {}€ | Mean of Rewards (Test): {} | Mean Total Electricity Bill (Test): {}€{}'.format(self.model_name, rounded_percentage, rounded_mean_reward_train, rounded_mean_bill_train, rounded_mean_reward_test, rounded_mean_bill_test, (10 * space)), end = '\r')

        torch.save(self.actor.state_dict(), f'{self.save_path}/{t}')
        
        self.writer.add_scalar("MeanReward/train", mean_reward_test, t)
        self.writer.add_scalar("MeanTotalBill/train", mean_bill_test, t)

    
    def _init_hyperparameters(self, timesteps_per_batch, timesteps_per_episode_mult):
        # Initialize default values for hyperparameters
        self.timesteps_per_batch = timesteps_per_batch                                          # Number of timesteps to run per batch
        self.max_timesteps_per_episode = timesteps_per_episode_mult * self.timesteps_per_batch  # Max number of timesteps per episode
        self.n_updates_per_iteration = 5                                                        # Number of times to update actor/critic per iteration
        self.lr = 0.005                                                                         # Learning rate of actor optimizer
        self.gamma = 0.95                                                                       # Discount factor to be applied when calculating Rewards-To-Go
        self.clip = 0.2                                                                         # Recommended 0.2, helps define the threshold to clip the ratio during SGD
        self.batch_size = timesteps_per_episode_mult * self.timesteps_per_batch